#encoding: utf-8
class Student < ActiveRecord::Base
  attr_accessible :email, :id, :name

  validates_presence_of :email, message: "Campo deve ser preenchido."
  validates_presence_of :id, message: "Campo deve ser preenchido."
  validates_presence_of :name, message: "Campo deve ser preenchido."

  validates_uniqueness_of :email, message: "Email já cadastrado."
  validates_uniqueness_of :id, message: "Número USP já cadastrado."

  has_many :books
end
