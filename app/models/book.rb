#encoding: utf-8
class Book < ActiveRecord::Base
  attr_accessible :author, :edition, :n_serie, :title

  validates_presence_of :author, message: "Campo deve ser preenchido."
  validates_presence_of :edition, message: "Campo deve ser preenchido."
  validates_presence_of :n_serie, message: "Campo deve ser preenchido."
  validates_presence_of :title, message: "Campo deve ser preenchido."

  validates_uniqueness_of :n_serie, message: "Número já cadastrado."

end
